"""
some test
"""
from character import Character
import objects


def test_character():
    hero = Character("RainMaker", "M", "Ogre", 25, 100, 1)
    print(hero.get_name())
    print(hero.get_race())
    print(hero.get_equip())
    hero.set_equip("legs", "jeans")
    print(hero.get_equip())
    print(hero.get_invent())
    hero.set_equip("legs", "armor")
    print(hero.get_invent())
    print(hero.get_equip())
    hero.add_in_invent('bague')
    hero.add_in_invent('sort')
    print(hero.get_invent())
    hero.del_in_invent('jeans')
    print(hero.get_invent())


def test_dico():
    dico = {"head": None, "arm": None, "chest": None, "legs": None, "hands": None, "feet": None}
    print(dico.__getitem__("head"))
    print(dico)


def test_list():
    test = [1, 2, 3]
    del test[test.index(2)]
    print(test)


def test_object():
    random = objects.Destructible("Tree", "/bdd/map/mesh/tree.fbx", 500)
    print(random.get_name())
