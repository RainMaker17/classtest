"""
Some classes for characters and monsters ingame
"""


class Character:
    """
    """
    def __init__(self, name, sexe, race, age, life, level):
        """
        Initialize a basic character
        """
        self.name = name
        self.sexe = sexe

        self.strenght = 0
        self.agility = 0
        self.intelligence = 0
        self.chance = 0
        self.wisdom = 0

        self.skill_list = ["strenght : " + str(self.strenght),
                           "agility : " + str(self.agility),
                           "intelligence : " + str(self.intelligence),
                           "chance : " + str(self.chance),
                           "Widom : " + str(self.wisdom)]

        self.race = race
        self.age = age
        self.invent = list()
        self.equipment = {"head": None, "arms": None, "chest": None, "hands": None, "legs": None, "feet": None}

        self.life = life
        self.level = level
        self.xp = 0.00
        self.skills_points = 5

    # --------------- Getter and Setter ----------------

    def get_skills(self):
        """
        return the character's skills
        """
        return self.skill_list

    def get_strenght(self):
        """
        return the character's strenght
        """
        return self.strenght

    def increase_strenght(self, value):
        """
        add the value to the skill : strenght
        """
        self.strenght += value

    def get_intelligence(self):
        """
        return the character's intelligence
        """
        return self.intelligence

    def increase_intelligence(self, value):
        """
        add the value to the skill : intelligence
        """
        self.intelligence += value

    def get_agility(self):
        """
        return the character's agility
        """
        return self.agility

    def increase_agility(self, value):
        """
        add the value to the skill : agility
        """
        self.agility += value

    def get_chance(self):
        """
        return the character's chance
        """
        return self.chance

    def increase_chance(self, value):
        """
        add the value to the skill : chance
        """
        self.chance += value

    def get_wisdom(self):
        """
        return the character's wisdom
        """
        return self.wisdom

    def increase_wisdom(self, value):
        """
        add the value to the skill : wisdom
        """
        self.wisdom += value

    def get_name(self):
        """
        return the character's name
        """
        return self.name
    
    def set_name(self, new_name):
        """
        """
        if not isinstance(new_name, str):
            return False
        self.name = new_name
        return True
    
    def get_sexe(self):
        """
        return the character's sexe
        """
        return self.sexe

    def get_race(self):
        """
        return the character's race
        """
        return self.race

    def get_level(self):
        """
        return the character's level
        """
        return self.level

    def add_level(self):
        """
        New level passed
        """
        self.level += 1

    def get_life(self):
        """
        return the character's life
        """
        return self.life

    def increase_life(self, new_life):
        """
        Increase the max life
        """
        self.life = new_life

    def decrease_life(self, nbr):
        """
        when the character's hit his life decrease
        """
        self.life += -nbr

    def get_xp(self):
        """
        return the character's experience
        """
        return self.xp

    def add_xp(self, nbr):
        """
        """
        self.xp += nbr

    def get_age(self):
        """
        return the character's age
        """
        return self.age

    def get_invent(self):
        """
        return the character's invent
        """
        return self.invent
    
    def add_in_invent(self, new_object):
        """
        """
        self.invent.append(new_object)
        return True

    def del_in_invent(self, elem):
        """
        delete 'elem' from the character's invent
        """
        self.get_invent().__delitem__(int(self.get_invent().index(elem)))
        return True

    def set_invent(self, invent_list):
        """
        set the owl invent for the new 'invent_list'
        """
        self.invent = invent_list
        return True

    def get_equip(self):
        """
        return the character's equipment
        """

        head = self.equipment["head"]
        arms = self.equipment["arms"]
        hands = self.equipment["hands"]
        chest = self.equipment["chest"]
        legs = self.equipment["legs"]
        feet = self.equipment["feet"]
        equip_list = [head, arms, hands, chest, legs, feet]
        return equip_list

    def set_equip(self, place, new_object):
        """
        change the equipment in "place" to "object"
        """
        if self.equipment.__getitem__(place) is not None:
            self.add_in_invent(self.equipment.__getitem__(place))
        self.equipment[place] = new_object
        return True

    # ------------ Other functions -------------
    def equip(self, equipment):
        """
        equip a piece and if the case is already taken the older piece get in invent
        """
        pass
    
    def level_up(self, xp):
        """
        """
        pass
