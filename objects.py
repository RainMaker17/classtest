"""
Some classes for objects
"""


class Object:
    def __init__(self, name, mesh):
        """
        :param name: object's name
        :param mesh: object's mesh path file
        """
        self.name = name
        self.mesh_path = mesh

    def get_name(self):
        return self.name

    def set_name(self, new_name):
        if isinstance(new_name, str):
            self.name = new_name
            return True
        return False

    def get_mesh(self):
        return self.mesh_path

    def set_mesh(self, new_path):
        self.mesh_path = new_path


class Destructible(Object):

    def __init__(self, name, mesh, resistance_max):
        """
        :param name: object name
        :param mesh: object mesh path file
        :param resistance_max: object resistance maximum
        """
        Object.__init__(self, name, mesh)
        self.resistance_max = resistance_max
        self.resistance = self.resistance_max

    def get_resistance_max(self):
        return self.resistance_max

    def set_resistance_max(self, new_resistance):
        self.resistance_max = new_resistance

    def get_resistance(self):
        return self.resistance

    def decrease_resistance(self, value):
        self.resistance += - value

    def increase_resistance(self, value):
        self.resistance += value


class Usable(Destructible):
    def __init__(self, name, mesh, resistance):
        Destructible.__init__(self, name, mesh, resistance)
        self.interaction = 0
        self.reparation_max = 75 / 100 * resistance

    def get_number_of_interaction(self):
        return self.interaction

    def reset_interaction(self):
        self.interaction = 0
